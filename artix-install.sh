:

setterm -repeat off

[[ $# -ne 2 ]] && { echo "Usage: $0 <init> <disk>"; exit 1; }

init=$1
disk=$2

setterm -repeat off

echo "Init: $init, disk: $disk"
echo "Press <ENTER> if correct."
read

disk=$(sed 's|/dev/||' <<<$disk)

[ -b /dev/$disk ] || { echo "/dev/$disk does not exist!"; exit 1; }

sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 24/' /etc/pacman.conf

dd if=/dev/zero of=/dev/$disk bs=64M count=1
(
echo o;
echo n;
echo p;
echo 1;
echo ;
echo ;
echo a;
echo w;
) | fdisk /dev/$disk

umount /mnt 2>/dev/null

mkfs.ext4 -L ROOT /dev/${disk}1

mount /dev/${disk}1 /mnt

mkdir -p /mnt/var/cache/pacman/pkg
echo '---> Attempting to rsync packages from host /tmp/pkg'

rsync -avu nous@10.0.2.2:/tmp/pkg /mnt/var/cache/pacman

basestrap /mnt base linux grub elogind-$init
fstabgen -U /mnt >> /mnt/etc/fstab

cat << EOF > /mnt/root/finalize.sh
:
echo -e "test\ntest" | passwd
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
exit
EOF

chmod +x /mnt/root/finalize.sh

artix-chroot /mnt /root/finalize.sh

echo '---> Attempting to rsync back packages το host /tmp/pkg'

rsync -avu /mnt/var/cache/pacman/pkg nous@10.0.2.2:/tmp/

echo "Root password: test"

